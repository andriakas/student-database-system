package com.example.demo1.bank;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class BankAccountConfig {

    @Bean
    CommandLineRunner commandLineRunner(
            BankAccountRepository repository) {
        return args -> {
            BankAccount PetrasPetraitis = new BankAccount(
                    1L,
                    "PetrasPetraitis",
                    123456789,
                    5000,
                    300
            );

            BankAccount JonasJonaitis = new BankAccount(
                    2L,
                    "JonasJonaitis",
                    987654321,
                    10000,
                    500
            );

            repository.saveAll(
                    List.of(PetrasPetraitis, JonasJonaitis)
            );
        };
    }
}
