package com.example.demo1.bank;

import javax.persistence.*;

@Entity
@Table
public class BankAccount {
    @Id
    @SequenceGenerator(
            name = "bankaccount_sequence",
            sequenceName = "bankaccount_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "bankaccount_sequence"
    )


    private Long id;
    private String name;
    private Integer accountnumber;
    private Integer balanceid;
    private Integer amount;

    public BankAccount(Long id) {
        this.id = id;
    }

    public BankAccount(Long id,
                       String name,
                       Integer accountnumber,
                       Integer balanceid,
                       Integer amount) {
        this.id = id;
        this.name = name;
        this.accountnumber = accountnumber;
        this.balanceid = balanceid;
        this.amount = amount;
    }

    public BankAccount(String name, Integer accountnumber, Integer balanceid, Integer amount) {
        this.name = name;
        this.accountnumber = accountnumber;
        this.balanceid = balanceid;
        this.amount = amount;
    }

    public BankAccount() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(Integer accountnumber) {
        this.accountnumber = accountnumber;
    }

    public Integer getBalanceid() {
        return balanceid;
    }

    public void setBalanceid(Integer balanceid) {
        this.balanceid = balanceid;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", accountnumber=" + accountnumber +
                ", balanceid=" + balanceid +
                ", amount=" + amount +
                '}';
    }
}





